# CasaHacker - Novo website - Django CMS

Novo site da CasaHacker criado com Django CMS e baseado no repo da [OKFN](https://github.com/okfn/website).

**Requisitos**

- Python 2.7 com virtualenv
- node.js

Libs do python (além daquela presentes no `requirements`)
- libxml
- libxslt
- libsasl2
- Python Imaging Library (PIL) dependencies, see [here](http://stackoverflow.com/a/21151777/3449709) for a quick ubuntu instructions.

## Rodando no ambiente local

**Criar um virtual env**

```bash
$ virtualenv casahackerdjango
$ source casahackerdjango/bin/activate
```

**Instalar requisitos**

```bash
$ pip install -r requirements.dev.txt
$ npm install
$ python manage.py migrate
$ python manage.py update_index
```

**Criar um superuser**
```bash
$ python manage.py createsuperuser
```

**Iniciar o servidor**
```bash
$ python manage.py runserver # dev
```

Agora basta acessar a página em `http://127.0.0.1:8000/`.


## Observações

Por se tratar de uma plataforma CMS, a maior parte do conteúdo (páginas, assets, menus, textos etc) ficam salvos em um banco de dados dentro do ambiente virtual. Dito isso, a maior parte do desenvolvimento deve ser feito diretamente em ambiente deployado. Mudanças feitas através da interface gráfica no seu ambiente local não serão salvos no git e consequentemente não serão propagados para os outros desenvolvedores.

Uma alternativa é subir um banco de dados remoto e todos os desenvolvedores "apontarem" seus respectivos projetos locais para esse banco.

Páginas e conteúdos podem ser criados através dos recursos nativos do CMS ou então através dos templates desenvolvidos pela OKFN aqui presentes.

Mudanças "estruturais", ou seja, coisas que não demandarão atualizações recorrentes (novos templates, rodapé, menu superior, estilos gerais) podem ser feitas diretamente no código.

## Próximos passos

- Acoplar um banco de dados no ambiente virtual (mysql ou postgresql, preferencialmente)

- Deployar em servidor remoto (Digital Ocean)

- Adicionar as páginas e conteúdos diretamente no ambiente remoto

- Associar o DNS
